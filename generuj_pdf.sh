#!/bin/bash


for i in `ls results/*.svg`
do
    printf "Processing $i... "
    inkscape -A "${i%%.*}.pdf" $i;
    printf "Done.\n"
done
