#!/usr/bin/env python3

import sys
import os
import logging
from lxml import etree
from io import StringIO, BytesIO

log = logging.getLogger(__name__)

OUTPUT_PATH="results/"
def main():
    if len(sys.argv) != 3:
        print("./generuj.py [plik z imionami i nazwiskami] [szablon svg]")
        return
    
    filepath, svgpath = sys.argv[1], sys.argv[2]
    names = open(filepath, 'r').readlines()

    svg = etree.parse(svgpath)


    for fullname in names:
        try:
            fullname = fullname.strip()
            name, surname = tuple(fullname.split(" "))
            print("Name:{}; Surname:{}".format(name, surname))
            nameElem = svg.xpath("//n:tspan[@id='tspan3414']", namespaces={'n': "http://www.w3.org/2000/svg"})[0]
            surnameElem = svg.xpath("//n:tspan[@id='tspan3416']", namespaces={'n': "http://www.w3.org/2000/svg"})[0]
            nameElem.text, surnameElem.text = name, surname
            result_filename = "{}.svg".format(fullname.replace(" ", "_"))
            os.makedirs(OUTPUT_PATH, exist_ok=True)
            result = open(os.path.join(OUTPUT_PATH, result_filename), 'wb')
            print(etree.tostring(svg))
            result.write(etree.tostring(svg))
            result.close()
        except Exception as e:
            log.error("Nieprawidłowo przetworzono: {}\n{}".format(fullname, e))



if __name__=="__main__":
    main()